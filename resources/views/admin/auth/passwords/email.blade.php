@extends('admin.layout.auth')

@section('meta-title')
Reset Password | {{ Config::get('app.app_name') }}
@endsection

<!-- Main Content -->
@section('content')
<div class="login-box">
  <div class="login-logo">
    <b>{{ Config::get('app.app_name') }}</b>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Reset Password</p>

    <form role="form" method="POST" action="{{ url('/admin/password/email') }}">
      {{ csrf_field() }}
      <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-4">&nbsp;</div>
        <!-- /.col -->
        <div class="col-xs-8 text-right">
          <button type="submit" class="btn btn-primary btn-flat">Send Password Reset Link</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="{{ route('admin.login') }}" class="text-center"><i class="fa fa-lock fa-fw"></i> Login</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
@endsection
