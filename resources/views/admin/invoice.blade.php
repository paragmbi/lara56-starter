<html>
<head>
  <style>
    @page { margin: 100px 25px;font-size:14px; }
    header { position: fixed; top:-105px; left: 0px; right: 0px; background-color:#fff; height: 50px; }
    footer { position: fixed; bottom: -60px; left: 0px; right: 0px; background-color:#0f62ac;width:100%;height:40px; }
	main {margin-top:10px;left: 0px; right: 0px;}
    /**/
	big { page-break-after: always; }
    big:last-child { page-break-after: never; }
	
	.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid #a8a8a8;
	vertical-align:top;
	}
	/**/
	
	table{
		border-collapse: collapse;
		vertical-align:top;
	}
  </style>
</head>
<body>
	<header>
		<table style="width:100%;border-bottom:2px solid #ed3237;padding:10px;">
		<tr>
			<td>
			<img src="https://www.matrixbricks.com/images/matrix-bricks-infotech-300x109.png">
			<!--<i class="fa fa-globe"></i> Raja-n-Raja.-->
			</td>
			<td>
			<table style="width:100%;vertical-align:top;text-align:right;">
			<tr><td>Order Code: #001</td></tr>
			<tr><td><small>Order Date: 20th June, 2018</small></td></tr>
			<tr><td><small>Printed On: 09th July, 2018</small></td></tr>
			</table>
			</td>
		</tr>
		</table>
	</header>	
  
  
	<footer>
		<small style="vertical-align:middle;float:right;color:#fff;padding:5px;">{{date('Y')}} &copy; Matrix Bricks Infotech LLP.<br> All Rights Reserved.</small>
	</footer>
		
  <main>
    <!--<p>page1</p>
    <p>page2></p>-->
	
		<table style="width:100%">
		<tr>
		
		<td style="width:30%;vertical-align:top;">
		
			<table  style="font-size:14px;width:100%;" border="1" cellpadding="5">
			  <tr>
			  <th style="border-bottom:2px solid #ed3237;color:#222d32;font-size:16px;text-align:left;">Customer Details
			  </th>
			  </tr>
			  
			  <tr>
			  <td>
				<table style="width:100%">
				  <tr>
				  <td>
					<strong>09th July, 2018</strong><br>
					<b>Reg. No: #1111</b><br>
					Address<br>
					Contact1: 9879879879<br>
					Contact2: 9999999999<br>
					Email: parag@matrixbricks.com	
				  </td>
				  </tr>
				</table>
			  </td>
			 </tr>
			</table>
		
			<table  style="font-size:14px;width:100%;" border="1" cellpadding="5">
			  <tr>
			  <th style="border-bottom:2px solid #ed3237;color:#222d32;font-size:16px;text-align:left;">Work Location
			  </th>
			  </tr>
			  
			  <tr>
			  <td>
				<table style="width:100%">
				  <tr>
				  <td>
					Village: Village<br>
					Designation: Designation<br>
					Company: Company<br>
					Business: Business<br>
					Building: Building
				  </td>
				  </tr>
				</table>
			  </td>
			 </tr>
			  
			  <tr>
			  <td>
				<table style="width:100%">
				  <tr>
				  <td>
					Flat No: Flat No.<br>
					Plot No: Plot No.<br>
					Wing: Wing<br>
					Road: Road<br>
					Area: Area<br>
					Pincode: 400710
				  </td>
				  </tr>
				</table>
			  </td>
			 </tr>
			  
			  <tr>
			  <td>
				<table style="width:100%">
				  <tr>
				  <td>
					Opposite: Opposite<br>
					Near: Near<br>
					Behind: Behind<br>
					Above: Above<br>
					Reference: Reference
				  </td>
				  </tr>
				</table>
			  </td>
			 </tr>
			 
			</table>		
        </td>
		
		<td style="width:70%;vertical-align:top;">
		
			<table  style="font-size:14px;width:100%;" border="1" cellpadding="5">
			  <tr>
			  <th style="border-bottom:2px solid #ed3237;color:#222d32;font-size:16px;text-align:left;">Order Summary
			  </th>
			  </tr>
			  <tr>
			  <td>
				<table class="print_table" role="grid" aria-describedby="example1_info" style="width:100%;text-align:left;font-size:12px;" id="" border="1">
					<thead>
						<tr>
						  <th style="width:20px;">Sr.No.</th>
						  <th>Task	</th>
						  <th style="width:120px;">Remark</th>
						  <th style="width:60px;">Status</th>
						  <th style="text-align:right;width:100px;">Amt.(In Rupees)</small></th>
						</tr>
					</thead>
					<tbody>
					<?php 
					for($i = 1; $i < 6; $i++){?>
					<tr>
						<td>{{ $i }}</td>
						<td>Title</td>
						<td>Remark</td>
						<td>Status</td>
						<td style="text-align:right;">1000/-</td>
					</tr>
					<?php } ?>
					<tr>
						<td style="text-align:right;" colspan="4">Total Amount: </td> 
						<td style="text-align:right;">1000/-</td>
					</tr>
					<tr>
						<td style="text-align:right;" colspan="4">Charges: </td> 
						<td style="text-align:right;">1000/-</td>
					</tr>
					<tr>
						<td style="text-align:right;" colspan="4">Final Amount: </td> 
						<td style="text-align:right;">1000/-</td>
					</tr>
					<tr>
						<td style="text-align:right;" style="text-align:right;" colspan="4">Total Paid: </td> 
						<td style="text-align:right;">1000/-</td>
					</tr>
					<tr>
						<td style="text-align:right;" colspan="4">Balance Amount: </td> 
						<td style="text-align:right;">1000/-</td>
					</tr>
					</tbody>
				</table>
			  </td>
			 </tr>
			</table>
			
			<?php /**/?>
				<table class="print_table" role="grid" aria-describedby="example1_info" style="width:100%;text-align:left;" id="" border="1">
				<tr>
				<td style="vertical-align:bottom;height:220px;">Matrix Bricks Infotech LLP.</td>
				</tr>
				</table>
			<?php /**/?>
		</td>
		
		</tr>
		</table>
		
		
  </main>
	
  
</body>
</html>