@extends('layout.app')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            @if(Auth::guard('user')->check())
                Logged in as {{Auth::guard('user')->user()->name}}<br>
            @endif
                Contact Us Page<br>
                route: {{ Request::route()->getName() }}
        </div>
    </div>
@endsection
