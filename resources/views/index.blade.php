@extends('layout.app')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            @if(Auth::guard('user')->check())
                Logged in as {{Auth::guard('user')->user()->name}}<br>
            @endif
                Home Page<br>
                route: {{ Route::current()->getName() }}
        </div>
    </div>
@endsection
